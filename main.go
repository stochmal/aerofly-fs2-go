package main

import (
	"./goearth"
	"bytes"
	"log"
	"net"
	"os"
)

func main() {
	address := ":49002"

	if len(os.Args) >= 2 {
		address = os.Args[1]
	}

	addr, err := net.ResolveUDPAddr("udp", address)
	if err != nil {
		log.Fatalln("Invalid address:", address, err)
	}
	conn, err := net.ListenUDP("udp", addr)
	if err != nil {
		log.Fatalln("Listener:", address, err)
	}

	log.Print("Socket listen on: ", address)

	buff := make([]byte, 1024)
	for {
		n, addr, err := conn.ReadFromUDP(buff)
		if err != nil {
			log.Println("<-", addr, "Message error:", err)
			continue
		}
		msg := bytes.TrimSpace(buff[:n])
		log.Printf("<- %q from %s", msg, addr)

		goearth.WriteXML()
	}
}
